# Tosia NES

Hi! This is my first attempt at trying to write a NES emulator in C++.

## Intro ##
Since I was a kid I always loved consoles. The realization that a group of engineers
have worked together to create a system with the only purpose of entertainment still
impresses me greatly. Out of all the possible reasons, creating something that allows
others to create software for people to have fun is - in my humble opinion - one of the most
impressive.<br>
While I didn't have many consoles when I was a kid (my parents preferred to spend their money
on _better_ things) apart from a NES, as soon as I got money I started buying my own.<br>
The first one I got was a SNES that I bought from a shop, agreeing with the owner
to pay in three installments with the little cash I had. After that many other systems came,
Gameboy Advance (my favourite!), Dreamcast, N64, Gamecube, etc...<br>
Prior to all that I always had a computer at home because my two older brothers are computer science engineers.
Thanks to that I remember playing on an old Z80, Amstrad CPC and some other systems.
<br>It does not matter how many years passed, I'm still interested in games and I'm still interested in _how_ these
systems work. So after all this time I decided that to satisfy my curiosity I should try to write my
own NES emulator and if I'm able to complete it - maybe - write some others. 

## Scope
The scope of this project is merely educational to myself. To keep expectations in line,
the basic idea is to emulate the 6502(2A03) CPU and later work on the PPU. Sound will be, for now, optional.
At the same time I will be documenting the process so this README should be updated with what documentation I'm following
as well as thoughts on the process. While collaboration is not something I'm considering, I really appreciate
feedback and suggestions on how to approach this project. Please feel free to use my contact details.

## Documentation links
- 14/08/2021 -- [NESDEV documentation](https://www.nesdev.com/NESDoc.pdf) 

## Blog posts